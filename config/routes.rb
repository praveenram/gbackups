Rails.application.routes.draw do
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'index#index'

  get '/mail', to: 'mail#index'
  get '/mail/index', to: 'mail#index'

  get '/mail/check_for_new_labels', to: 'mail#check_for_new_labels'
  get '/mail/update_labels', to: 'mail#update_labels'

  get '/mail/update_label/:label', to: 'mail#update_label', as: 'update_label'

  get '/mail/get_messages/:label/:page', to: 'mail#get_messages', as: 'get_messages_by_label'

  get '/mail/check_for_new_messages/:label_id', to: 'mail#check_for_new_messages'
  get '/mail/sync_messages/:label_id(/:page_token)', to: 'mail#sync_messages'
  get '/mail/get_synced_messages/:label_id', to: 'mail#get_synced_messages'
  get '/mail/get_message_from_server/:message_id', to: 'mail#get_message_from_server'

  get '/mail/view/:id', to: 'mail#view'

  devise_for :users, :controllers => { omniauth_callbacks: 'users/omniauth_callbacks', sessions: 'users/sessions' }
end
var tenMins = 10 * 60 * 1000;
var labelCount = {};

var LabelsManager = function () {
  var refreshInterval = tenMins;
  var refreshCountCallBack = undefined;
  var checkForNewLabelsCallBack = undefined;

  var newLabelsFoundNotification = 'newLabelsFound';
  var downloadMessagesForLabelNotification = 'syncMessageFor';

  var refreshLabels = function () {
    var oldCount = {}
    $.extend(true, oldCount, labelCount);
    $(".labels-list .label-name").each(function(index, label) {
      var labelId = $(label).data("unique-id").trim();
      $(".labels-list .label-" + labelId + ".label-count").html('<i class="fa fa-spinner"></i>');
      $.get("/mail/update_label/" + encodeURIComponent(labelId), function(data, status, response) {
        $(".labels-list .label-" + labelId + ".label-name").text(data.name);
        $(".labels-list .label-" + labelId + ".label-name").attr("title", data.name);
        $(".labels-list .label-" + labelId + ".label-count").text(data.messages_total);
        addNotificationsForNewMessages(oldCount, labelId, data.messages_total, data.name);
      });
    });
    resetCountAutoRefresh();
  };

  var addNotificationsForNewMessages = function (oldCount, key, newCount, name) {
    if(newCount - oldCount[key] > 0) {
      notificationCenter.addNotification(downloadMessagesForLabelNotification + key, "New Messages found in " + name + ". Would you like to download messages from this label?", Mail.backgroundDownloadMessages);
    }
  };

  var updateLabelsPane = function (event) {
    $.get("/mail/update_labels", function (data, status, response) {
      $(".labels-list").append(data["html"]);
      $.each(data["objects"], function (index, labelObj) {
        notificationCenter.addNotification(downloadMessagesForLabelNotification + labelObj["unique_id"], "New Label: " + labelObj["name"] + " found. Would you like to download messages from this label?", Mail.backgroundDownloadMessages);
      });
    });
    initLabelClickEventListener();
    refreshLabels();
  };

  var checkForLabelUpdates = function () {
    $.get("/mail/check_for_new_labels", function(data, status, response) {
      if(parseInt(data) > 0) {
        updateLabelsPane();
      }
    });
    resetNewLabelAutoRefresh();
  };

  var resetCountAutoRefresh = function () {
    clearInterval(refreshCountCallBack);
    setupCountAutoRefresh();
  };

  var resetNewLabelAutoRefresh = function () {
    clearInterval(checkForNewLabelsCallBack);
    setupNewLabelCheck();
  };

  var setupCountAutoRefresh = function () {
    refreshCountCallBack = setInterval(refreshLabels, refreshInterval);
  };

  var setupNewLabelCheck = function () {
    checkForNewLabelsCallBack = setInterval(checkForLabelUpdates, refreshInterval);
  };

  var initLabelClickEventListener = function () {
    $(".labels-list > .row.mail-label").off();
    $(".labels-list > .row.mail-label").on('click', Mail.loadLabelMessages);
  };

  var loadMessagesFromFirstLabel = function () {
    $($('.labels-list > .row.mail-label')[0]).trigger('click');
  };

  this.init = function () {
    if(Cookie.isGoogleLogin()) {
      $(".label-refresh-count").removeClass('hide');
      $(".update-labels").removeClass('hide');
      $(".label-refresh-count").on('click', refreshLabels);
      $(".update-labels").on('click', checkForLabelUpdates);
      setupCountAutoRefresh();
      setupNewLabelCheck();
    } else {
      $('.label-refresh-count').addClass('hide');
      $('.update-labels').addClass('hide');
    }
    initLabelClickEventListener();
    loadMessagesFromFirstLabel();
  };
};

$(document).ready(function () {
  new LabelsManager().init();
});
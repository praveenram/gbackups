var messages = {};

var Mail = {
  loadMessages: function (labelId, pageNumber) {
    $.get("/mail/get_messages/" + encodeURIComponent(labelId) + '/' + pageNumber, function(htmlData, status, response) {
      $(".messages-list").html(htmlData);
      Mail.bindEvents();
      Mail.bindPageEvents();
    });
  },
  loadLabelMessages: function (event) {
    var labelId = $(event.currentTarget).find(".label-name").data("unique-id").trim();
    $(".labels-list > .row.mail-label").removeClass('selected');
    $(event.currentTarget).addClass('selected');
    Mail.loadMessages(labelId, 1);
  },

  syncMessages: function (labelId) {
    $.get('/mail/get_synced_messages/' + labelId).
      success(function (data, status, response) {
        messages[labelId] = data;
        Mail.backgroundSyncMessages(labelId, null);
      });
  },

  backgroundSyncMessages: function (labelId, pageToken) {
    if(Cookie.isGoogleLogin()) {
      var optionalPageToken = pageToken === null ? '' : '/' + pageToken;
      $.get('/mail/sync_messages/' + labelId + optionalPageToken).
        success(function (data, status, response) {
	  if(data["messages"] !== undefined) {
            $.each(data["messages"], function (index, message) {
              messageId = message["id"];
              if(messages[labelId][messageId] == undefined || messages[labelId][messageId] == null) {
                downloadQueue.add(messageId);
              }
            });
	  }
          nextPageToken = data["next_page_token"];
          if(nextPageToken !== '' && nextPageToken !== null && nextPageToken !== undefined) {
            setTimeout(function () {
              Mail.backgroundSyncMessages(labelId, nextPageToken);
            }, 0);
          }
        }).
        error(function (data, status, response) {

        });
    }
  },
  backgroundDownloadMessages: function (labelId) {
    if(Cookie.isGoogleLogin()) {
      $.get('/mail/check_for_new_messages/' + labelId, function (data, status, response) {
        $.each(data, function (index, messageId) {
          downloadQueue.add(messageId);
        });
      });
    }
  },
  viewMessage: function (messageId) {
    $.get("/mail/view/" + messageId).
      success(function (data, status, response) {
        $("#messageView .header").html(data["header"]);
        $("#messageView .content").html(data["content"]);
        $('#messageViewModal').modal('show');
      }).error(function (data, status, response) {
        console.log(data);
      });
  },
  bindEvents: function () {
    $(".messages-list > .row.box").each(function (index, element) {
      $(element).off();
      $(element).on('click', function (event) {
        var messageId = $(event.currentTarget).data("message-id");
        Mail.viewMessage(messageId);
      });
    });
  },
  bindPageEvents: function () {
    $('.messages-pagination .page').off();
    $('.messages-pagination .page').on('click', function (event){
      var pageNum = $(event.currentTarget).text().trim();
      var currentLabel = $(".labels-list > .row.mail-label.selected").find('.label-name').data('unique-id').trim();
      Mail.loadMessages(currentLabel, pageNum);
    });
  }
};

var Notify = function () {
  var notificationMessages = {};

  this.enableAlert = function () {
    $("#notify").addClass("blink");
  };

  this.disableAlert = function () {
    var count = 0;
    $.each(notificationMessages, function(key, value){
      if(value !== undefined || value !== null) {
        count+=1;
      }
    });
    if(count == 0)
      $("#notify").removeClass("blink");
  };

  this.addNotification = function (notificationId, message, action) {
    notificationMessages[notificationId] = {message: message, action: action};
    this.enableAlert();
    if(($("#notificationsModal").data('bs.modal') || {isShown: false}).isShown) {
      displayNotificationInModal(notificationId, notificationMessages[notificationId]);
      this.bindEvents();
    }
  };

  var displayNotificationInModal = function (notificationId, notification) {
    $("#notificationsModal .notifications").loadTemplate($("#notificationTemplate"), {
      message: notification["message"],
      notificationId: notificationId
    }, {
      append: true
    });
  };

  this.showNotifications = function () {
    $("#notificationsModal .notifications").html('');
    $.each(notificationMessages, function (notificationId, notification) {
      displayNotificationInModal(notificationId, notification);
    });
    this.bindEvents();
    $('#notificationsModal').modal('show');
  };

  var removeNotification = function(notificationId) {
    delete notificationMessages[notificationId];
    $("#notificationsModal .notifications").find(".row." + notificationId).remove();
  };

  this.bindEvents = function () {
    var self = this;
    $.each(notificationMessages, function (notificationId, notification) {
      $("#notificationsModal .notifications .confirm." + notificationId).off();
      $("#notificationsModal .notifications .confirm." + notificationId).on('click', function(event) {
        labelId = notificationId.split("syncMessageFor")[1];
        notification["action"](labelId);
        removeNotification(notificationId);
        self.disableAlert();
      });
      $("#notificationsModal .notifications .reject." + notificationId).off();
      $("#notificationsModal .notifications .reject." + notificationId).on('click', function(event) {
        removeNotification(notificationId);
        self.disableAlert();
      });
    });
  };

  this.init = function () {
    var self = this;
    $("#notify").on('click', function () {
      self.showNotifications();
    });
  };
};

var notificationCenter = new Notify();

$(document).ready(function () {
  notificationCenter.init();
});
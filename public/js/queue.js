var MessageDownloadQueue = function () {
  var queue = [];
  var activeJobs = {};
  var maxParallelJobs = 500;
  var activeJobCount = 0;

  var downloadMessage = function(messageId) {
    if(Cookie.isGoogleLogin()) {
      $.get("/mail/get_message_from_server/" + messageId).success(function (data, status, response) {
        //console.log(data);
        activeJobCount--;
        delete activeJobs[messageId];
        if(activeJobCount == 0 && queue.length == 0) {
          $("#queue").removeClass("blink");
          window.onbeforeunload = undefined;
        }
      }).error(function (data, status, response) {
        //Can add logic to retry.
        console.log("Download of message " + messageId + " failed!");
        activeJobCount--;
        delete activeJobs[messageId];
        if(activeJobCount == 0 && queue.length == 0) {
          $("#queue").removeClass("blink");
          window.onbeforeunload = undefined;
        }
      });
    }
  };
  
  this.add = function (id) {
    queue.push(id);
  };

  var animateDownloadIcon = function () {
    $("#queue").addClass("blink");
  };

  var download = function () {
    while(activeJobCount <= maxParallelJobs && queue.length > 0) {
      animateDownloadIcon();
      id = queue.shift();
      activeJobs[id] = true;
      activeJobCount++;
      downloadMessage(id);
      window.onbeforeunload = function () {
        return "Are you sure you want to move away from this page? Messages are being downloaded in the background. Sync might stop if you navigate away from the page."
      };
    }
  };

  setInterval(download, 0.1 * 60 * 1000);
};

var downloadQueue = new MessageDownloadQueue();
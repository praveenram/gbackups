class AddUniqueConstraintOnMessageId < ActiveRecord::Migration
  def change
    add_index :messages, [:unique_id, :user_id], unique: true
  end
end
class RenameTableLabelMessages < ActiveRecord::Migration
  def change
    rename_table :label_messages, :labels_messages
  end
end

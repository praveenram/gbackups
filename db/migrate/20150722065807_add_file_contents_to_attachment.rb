class AddFileContentsToAttachment < ActiveRecord::Migration
  def change
    add_column :attachments, :file_contents, :text
  end
end

class CreateAccessTokens < ActiveRecord::Migration
  def change
    create_table :access_tokens do |t|
      t.string :access_token
      t.string :refresh_token
      t.datetime :expires_at
      t.belongs_to :user
      t.timestamps null: false
    end
  end
end

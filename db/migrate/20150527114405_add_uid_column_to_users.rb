class AddUidColumnToUsers < ActiveRecord::Migration
  def change
    add_column User, :provider, :string, null: false
    add_column User, :uid, :string, null: false
  end
end
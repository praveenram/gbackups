class CreateAttachment < ActiveRecord::Migration
  def change
    create_table :attachments do |t|
      t.string :unique_id
      t.integer :message_part_body_id
    end
  end
end
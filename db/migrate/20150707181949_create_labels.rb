class CreateLabels < ActiveRecord::Migration
  def change
    create_table :labels do |t|
      t.integer :user_id
      t.string :unique_id
      t.string :label_list_visibility
      t.string :message_list_visibility
      t.integer :messages_total
      t.integer :messages_unread
      t.string :name
      t.integer :threads_total
      t.integer :threads_unread
      t.string :label_type
    end
  end
end
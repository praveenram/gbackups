class RemoveColumnsDataAttachmentIdAndSizeFromMessagePart < ActiveRecord::Migration
  def change
    remove_column :message_parts, :data
    remove_column :message_parts, :attachment_id
    remove_column :message_parts, :size
  end
end
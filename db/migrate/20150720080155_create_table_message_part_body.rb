class CreateTableMessagePartBody < ActiveRecord::Migration
  def change
    create_table :message_part_bodies do |t|
      t.string :data
      t.integer :size
      t.integer :message_part_id
    end
  end
end

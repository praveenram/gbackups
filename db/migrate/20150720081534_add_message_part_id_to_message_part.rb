class AddMessagePartIdToMessagePart < ActiveRecord::Migration
  def change
    add_column :message_parts, :message_part_id, :integer
  end
end

class CreateLabelMessages < ActiveRecord::Migration
  def change
    create_table :label_messages, id: false  do |t|
      t.integer :label_id
      t.integer :message_id
    end
  end
end
class CreateMessagePart < ActiveRecord::Migration
  def change
    create_table :message_parts do |t|
      t.string :data
      t.string :filename
      t.string :attachment_id
      t.string :mime_type
      t.integer :size
      t.string :part_id
      t.integer :message_id
    end
    add_column :message_parts, :headers, :hstore
  end
end

class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.integer :user_id

      ## Google Identifiers
      t.string :history_id
      t.string :unique_id
      t.string :internal_date

      t.string :raw
      t.integer :size_estimate
      t.string :snippet
      t.string :thread_id
    end
  end
end

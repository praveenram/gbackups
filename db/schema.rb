# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150722065807) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"

  create_table "access_tokens", force: :cascade do |t|
    t.string   "access_token"
    t.string   "refresh_token"
    t.datetime "expires_at"
    t.integer  "user_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "attachments", force: :cascade do |t|
    t.string   "unique_id"
    t.integer  "message_part_body_id"
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.text     "file_contents"
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "labels", force: :cascade do |t|
    t.integer "user_id"
    t.string  "unique_id"
    t.string  "label_list_visibility"
    t.string  "message_list_visibility"
    t.integer "messages_total"
    t.integer "messages_unread"
    t.string  "name"
    t.integer "threads_total"
    t.integer "threads_unread"
    t.string  "label_type"
  end

  create_table "labels_messages", id: false, force: :cascade do |t|
    t.integer "label_id"
    t.integer "message_id"
  end

  create_table "message_part_bodies", force: :cascade do |t|
    t.string  "data"
    t.integer "size"
    t.integer "message_part_id"
  end

  create_table "message_parts", force: :cascade do |t|
    t.string  "filename"
    t.string  "mime_type"
    t.string  "part_id"
    t.integer "message_id"
    t.hstore  "headers"
    t.integer "message_part_id"
  end

  create_table "messages", force: :cascade do |t|
    t.integer "user_id"
    t.string  "history_id"
    t.string  "unique_id"
    t.string  "internal_date"
    t.string  "raw"
    t.integer "size_estimate"
    t.string  "snippet"
    t.string  "thread_id"
  end

  add_index "messages", ["unique_id", "user_id"], name: "index_messages_on_unique_id_and_user_id", unique: true, using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "provider",                            null: false
    t.string   "uid",                                 null: false
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end

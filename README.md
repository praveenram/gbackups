## Google Backup Tool

Helps in backing up and searching across archives of backed up data.

### Done:
* Backup / Sync with GMail
* Basic Interface to View Emails
* Postgres for user accounts and storing email content.

### WIP:
* Search
* Move from CarrierWave to Mongo for File store Solution
* Encryption of Data
* Export Backup option

### Other Google Services to add:
* GDrive
* GCalendar
class Users::SessionsController < Devise::SessionsController
  def create
    cookies.delete :google_login
    super
  end

  def destroy
    cookies.delete :google_login
    super
  end
end
class MailController < ApplicationController
  before_filter :authenticate_user!
  before_filter :assign_user

  PER_PAGE = 25

  def index
    @labels = Label.where(user_id: current_user.id)
    @messages = []
  end

  def get_messages
    label = Label.find_by(unique_id: params[:label], user_id: current_user.id)
    messages = []
    message_query = label.messages.where(user_id: current_user.id)
    total_count = message_query.count
    message_query.select(:id, :internal_date).order(internal_date: :desc).page(params[:page]).per(PER_PAGE).each do |message|
      headers = message.headers
      messages << { id: message.id, from: headers["From"], subject: headers["Subject"], date: message.internal_date.to_i }
    end
    render partial: 'message_list', locals: { messages: messages, total_pages: (total_count.to_f/PER_PAGE.to_f).ceil, page_no: params[:page] }
  end

  def update_label
    label = Label.find_by(unique_id: params[:label], user_id: current_user.id)
    label.update
    render json: label.to_json
  end

  def check_for_new_labels
    pending_labels_to_sync = get_new_labels_on_server.size
    render text: pending_labels_to_sync
  end

  def update_labels
    labels_to_sync = get_new_labels_on_server
    labels_to_sync.each do |labelId|
      label = Label.get_label_details(labelId)
      Label.create_from(label)
    end

    new_labels = Label.where(unique_id: labels_to_sync).where(user_id: current_user.id)

    new_labels_html = ""
    new_labels.each do |label|
      new_labels_html += render_to_string partial: 'label_entry', locals: {label: label}, layout: false
    end

    render json: {html: new_labels_html, objects: new_labels}.to_json
  end

  def sync_messages
    label_id = params[:label_id]
    page_token = params[:page_token]
    render json: Message.on_server(label_id, page_token)
  end

  def get_synced_messages
    label_id = params[:label_id]
    label = Label.find_by(user_id: current_user.id, unique_id: label_id)
    message_ids = {}
    label.messages.select(:unique_id).map do |message| message_ids.store(message.unique_id, true) end
    render json: message_ids
  end

  def check_for_new_messages
    label_id = params[:label_id]
    #Assumption is that labels will not be removed from a message, count of messages per label will always increase 
    render json: get_new_messages_from_server(label_id)
  end

  def get_message_from_server
    message_id = params[:message_id]
    gmessage = Message.get_message_details(message_id)
    message = Message.create_from(gmessage)
    render json: message.to_json
  end

  def view
    message_id = params[:id]
    message = Message.includes(:message_parts).find(message_id)
    header_html = render_to_string partial: 'message_header', locals: { subject: message.headers["Subject"], from: message.headers["From"], date: message.internal_date.to_i }

    content = message.html_view
    attachments = message.attachments

    content_html = render_to_string partial: 'message_content', locals: { content: content, attachments: attachments }
    return render json: {header: header_html, content: content_html}.to_json
  end

  private
  def assign_user
    Label.current_user = current_user
    Message.current_user = current_user
    Attachment.current_user = current_user
  end

  def get_new_labels_on_server
    label_ids = Label.all_on_server.map do |label| label.id end
    synced_label_ids = Label.select(:unique_id).where(user_id: current_user.id).map do |label| label.unique_id end
    label_ids - synced_label_ids
  end

  def get_new_messages_from_server(label_id)
    message_ids = Message.all_on_server(label_id).map do |message| message.id end
    label = Label.find_by(unique_id: label_id, user_id: current_user.id)
    synced_message_ids = label.messages.select(:unique_id).map do |message| message.unique_id end
    message_ids - synced_message_ids
  end
end
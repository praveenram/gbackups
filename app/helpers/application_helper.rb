module ApplicationHelper
  def format_time(epochMilliseconds)
    Time.at(epochMilliseconds / 1000).strftime("%d %b, %Y - %H:%M %Z")
  end
end
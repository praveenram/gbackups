require 'fileutils'

class AttachmentDownloadJob < Struct.new(:id, :unique_id, :message_id, :filename, :encoding, :current_user)
  def perform
    p "*"*80
    p "Attachment: #{id}"
    p encoding
    p filename

    if unique_id.nil?
      return
    end

    mail_service = MailService.new(current_user).get_mail_service
    response = mail_service.get_user_message_attachment(Gbackup::CURRENT_USER_ID, message_id, unique_id)

    ext = File.extname(filename).gsub('.','')
    filename.gsub!(".#{ext}", ".#{ext.downcase}")
    p filename

    file = File.new(filename, 'w+')
    if ["pgp-signature", "js", "pem", "sh", "html", "yml", "ics", "patch"].include?(ext.downcase)
      response.data.force_encoding("ISO-8859-1").encode("UTF-8")
    else
      file.binmode
    end
    file << decoded(response.data)
    file.close

    file = File.open(filename, 'r')
    Attachment.find(id).update_attributes!(file: file)
    file.close
  end

  private
  def decoded(raw_source)
    if filename.include?(".eml")
      return Mail::Encodings.get_encoding(encoding).decode(raw_source)
    end
    raw_source
  end
end
require 'fileutils'

class MessagePartBody < ActiveRecord::Base
  belongs_to :message_part

  has_one :attachment
  accepts_nested_attributes_for :attachment

  def self.to_hash(body, filename, mime_type, headers)
    data = body.data || ''
    body_hash = {
      size: body.size
    }
    if !body.attachment_id.nil?
      body_hash.store(:attachment_attributes, {unique_id: body.attachment_id})
    elsif !mime_type.nil? && mime_type.include?("image/")
      p filename
      p headers
      filename = headers["Content-ID"].gsub!('<','').gsub('>','') if filename.empty?
      if filename.start_with?('~')
        filename = filename[0].gsub('~', '_') + filename[1, filename.length]
      end
      ext = File.extname(filename).gsub('.','')
      filename.gsub!(".#{ext}", ".#{ext.downcase}")

      file = File.new(filename, 'w+')
        file.binmode
        file << body.data
      file.close

      body_hash.store(:attachment_attributes, {file: File.open(filename)})
    else
      data.force_encoding("ISO-8859-1").encode("UTF-8")
      body_hash.store(:data, data)
    end
    body_hash
  end
end
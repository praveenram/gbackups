class MailService
  @@mail_service = nil

  def initialize(current_user)
    connect_to_mail_service(current_user.session_access_token)
  end

  def get_mail_service
    @@mail_service
  end

  def connect_to_mail_service(current_user_token)
    mail_service = @@mail_service || Google::Apis::GmailV1::GmailService.new
    mail_service.authorization = get_google_authorization(current_user_token)
    @@mail_service = mail_service
  end

  private
  def get_google_authorization(user_access_token)
    client_secrets = Google::APIClient::ClientSecrets.load('lib/client_secrets.json')
    auth = client_secrets.to_authorization
    auth.update_token!(access_token: user_access_token)
    auth
  end
end
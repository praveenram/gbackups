class Label < ActiveRecord::Base
  belongs_to :user

  cattr_accessor :current_user

  has_and_belongs_to_many :messages

  def update
    update_label
  end

  def self.all_on_server
    mail_service.list_user_labels(Gbackup::CURRENT_USER_ID).labels
  end

  def self.create_from(label)
    create!(Label.to_hash(label, current_user.id))
  end

  def self.get_label_details(label_id)
    mail_service.get_user_label(Gbackup::CURRENT_USER_ID, label_id)
  end

  def to_json
    {
      name: name,
      messages_total: messages_total
    }.to_json
  end

  private
  def self.to_hash(labelResponse, current_user_id)
    {
      user_id: current_user_id,
      unique_id: labelResponse.id,
      label_list_visibility: labelResponse.label_list_visibility,
      message_list_visibility: labelResponse.message_list_visibility,
      messages_total: labelResponse.messages_total,
      messages_unread: labelResponse.messages_unread,
      name: labelResponse.name,
      threads_total: labelResponse.threads_total,
      threads_unread: labelResponse.threads_unread,
      label_type: labelResponse.type
    }
  end

  def update_label(label=self)
    labelResponse = mail_service.get_user_label(Gbackup::CURRENT_USER_ID, label.unique_id)
    label.update_attributes!(Label.to_hash(labelResponse, Label.current_user.id))
  end

  def mail_service
    MailService.new(Label.current_user).get_mail_service
  end

  def self.mail_service
    MailService.new(Label.current_user).get_mail_service
  end
end
class UpdateAccessTokenJob < Struct.new(:id)
  def perform
    token = AccessToken.find(id)
    response = RestClient.post 'https://www.googleapis.com/oauth2/v3/token', client_id: Gbackup::OMNIAUTH['google']['client_id'],client_secret: Gbackup::OMNIAUTH['google']['client_secret'],refresh_token: token.refresh_token,grant_type: "refresh_token"
    response = JSON.parse(response)
    p response
    token.access_token = response["access_token"]
    token.expires_at = Time.now + response["expires_in"].seconds
    token.save!
  end
end
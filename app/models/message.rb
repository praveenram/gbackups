class Message < ActiveRecord::Base
  belongs_to :user

  has_and_belongs_to_many :labels
  has_many :message_parts

  accepts_nested_attributes_for :message_parts

  cattr_accessor :current_user

  def self.on_server(label_id, page_token)
    mail_service.list_user_messages(Gbackup::CURRENT_USER_ID, include_spam_trash: true, label_ids: [label_id], page_token: page_token)
  end

  def self.all_on_server(label_id)
    gmessages = mail_service.list_user_messages(Gbackup::CURRENT_USER_ID, include_spam_trash: true, label_ids: [label_id])
    messages = get_all_messages(gmessages, label_id)
    messages || []
  end

  def self.get_all_messages(gmessages, label_id)
    messages = gmessages.messages
    unless gmessages.next_page_token.nil?
      gmessages = mail_service.list_user_messages(Gbackup::CURRENT_USER_ID, include_spam_trash: true, label_ids: [label_id], page_token: gmessages.next_page_token)
      more_messages = get_all_messages(gmessages, label_id)
      messages.concat(more_messages) unless more_messages.nil?
    end
    messages
  end

  def self.get_message_details(message_id)
    mail_service.get_user_message(Gbackup::CURRENT_USER_ID, message_id)
  end

  def self.create_from(gmessage)
    message = Message.find_by(unique_id: gmessage.id, user_id: Message.current_user.id)
    if(message.nil?)
      message = Message.new(Message.to_hash(gmessage, Message.current_user.id))
      update_labels(message, gmessage)
      message.save!
    else
      update_labels(message, gmessage)
      message.save!
    end
    message
  end

  def self.update_labels(message, gmessage)
    label_ids = []
    message.labels.map do |label| label_ids.push(label.unique_id) end
    pending_labels = gmessage.label_ids - label_ids
    if pending_labels.count > 0
      pending_labels.each do |id|
        label = Label.find_by(unique_id: id, user_id: current_user.id)
        message.labels << label unless label.nil?
      end
    end
  end

  def headers
    MessagePart.find_by(message_id: self.id).headers
  end

  def html_view
    payload = message_parts.first
    html = payload.message_content("text/html")
    if html.empty?
      html = payload.message_content("text/plain")
    end
    inline_content = payload.inline_content
    inline_content.each do |inline_mod|
      html.gsub!("cid:#{inline_mod[:content_id]}", inline_mod[:url])
    end
    html
  end

  def attachments
    payload = message_parts.first
    payload.attachments
  end

  private
  def self.to_hash(gmessage, current_user_id)
    {
      user_id: current_user_id,
      history_id: gmessage.history_id,
      unique_id: gmessage.id,
      internal_date: gmessage.internal_date,
      raw: gmessage.raw,
      size_estimate: gmessage.size_estimate,
      snippet: gmessage.snippet,
      thread_id: gmessage.thread_id,
      message_parts_attributes: [MessagePart.to_hash(gmessage.payload)]
    }
  end

  def mail_service
    MailService.new(Message.current_user).get_mail_service
  end

  def self.mail_service
    MailService.new(Message.current_user).get_mail_service
  end
end
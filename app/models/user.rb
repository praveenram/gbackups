class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  devise :omniauthable, :omniauth_providers => [:google_oauth2]

  has_one :access_token
  accepts_nested_attributes_for :access_token

  validates_presence_of :access_token

  def session_access_token
    access_token.access_token
  end

  def self.find_for_google_oauth2(auth_hash, signed_in_resource=nil)
    user_data = auth_hash.info
    user = User.where(:email => user_data["email"]).first

    tokens = auth_hash.credentials

    #Uncomment the section below if you want users to be created if they don't exist
    unless user
      uid = auth_hash.uid
      password = Devise.friendly_token[0,20]
      access_token_params = { access_token: tokens["token"], refresh_token: tokens["refresh_token"], expires_at: Time.at(tokens["expires_at"]).to_datetime }
      user = User.create(email: user_data["email"], password: password, provider: :google_oauth2, uid: uid, access_token_attributes: access_token_params)
    else
      User.update_access_token(user, tokens)
    end
    user
  end

  def self.update_access_token(user, tokens)
    access_token_params = { access_token: tokens["token"], refresh_token: tokens["refresh_token"], expires_at: Time.at(tokens["expires_at"]).to_datetime }
    user.access_token.update_attributes!(access_token_params)
  end
end
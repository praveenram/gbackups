class MessagePart < ActiveRecord::Base  
  belongs_to :message

  has_one :message_part_body
  accepts_nested_attributes_for :message_part_body

  belongs_to :message_part, class_name: "MessagePart"
  has_many :parts, foreign_key: "message_part_id", class_name: "MessagePart"
  accepts_nested_attributes_for :parts

  def self.to_hash(part)
    headers = {}
    unless part.headers.nil?
      part.headers.each do |header|
        headers.store(header.name, header.value)
      end
    end
    part_hash = {
      part_id: part.part_id,
      filename: part.filename,
      mime_type: part.mime_type,
      headers: headers
    }
    unless part.body.nil?
      body_hash = MessagePartBody.to_hash(part.body, part.filename, part.mime_type, headers)
      part_hash.store(:message_part_body_attributes, body_hash)
    end
    unless part.parts.nil?
      parts = []
      part.parts.each do |sub_part|
        parts.push(MessagePart.to_hash(sub_part))
      end
      part_hash.store(:parts_attributes, parts)
    end
    part_hash
  end

  #improve logic to prevent multiple iterations over the message part structure
  def message_content(content_type)
    text = ""
    num_of_parts = parts.count
    if num_of_parts == 0 && !mime_type.nil? && mime_type.include?(content_type)
      p headers
      text += message_part_body.data
    else
      parts.each do |part|
        text+= part.message_content(content_type)
      end
    end
    text
  end

  def inline_content
    contents = []
    num_of_parts = parts.count
    content_disposition = headers["Content-Disposition"]
    if num_of_parts == 0 && content_disposition == "INLINE"
      p headers
      content_id = headers["Content-ID"].gsub!('<','').gsub!('>','')
      url = message_part_body.attachment.file.url
      contents.concat([{ content_id: content_id, url: url }])
    else
      parts.each do |part|
        val = part.inline_content
        contents.concat(val) if val.length > 0
      end
    end
    contents
  end

  def attachments
    attachments_list = []
    num_of_parts = parts.count
    content_disposition = headers["Content-Disposition"]
    p headers
    if num_of_parts == 0 && !content_disposition.nil? && content_disposition.include?("attachment")
      attachment = message_part_body.attachment
      url = attachment.file.url
      filename = attachment.file_file_name
      attachments_list.concat([{filename: filename, url: url}])
    else
      parts.each do |part|
        val = part.attachments
        attachments_list.concat(val) if val.length > 0
      end
    end
    attachments_list
  end
end
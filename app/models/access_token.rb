class AccessToken < ActiveRecord::Base
  belongs_to :user

  after_commit :refresh_access_token

  private
  def refresh_access_token
    run_in = (expires_at - Time.now.utc) - 300.seconds
    Delayed::Job.enqueue UpdateAccessTokenJob.new(id), 0, run_in.seconds.from_now.getutc
  end
end
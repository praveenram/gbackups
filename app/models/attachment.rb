class Attachment < ActiveRecord::Base
  cattr_accessor :current_user

  belongs_to :message_part_body

  mount_uploader :file, AttachmentUploader, mount_on: :file_file_name

  after_create :download_attachment

  private
  def filename
    name = message_part_body.message_part.filename
    if name.start_with?('~')
      name = name[0].gsub('~', '_') + name[1, name.length]
    end

    ext = File.extname(name).gsub('.','')
    if name.empty?
      name = "untitled"
    end
    if ext.empty?
      p "its empty!"
      ext = mime_type.split('/')[1] if mime_type.include?("image")
      ext = "txt" if mime_type.include?("text/plain")
      ext = "html" if mime_type.include?("text/html")
      ext = "eml" if mime_type.include? 'rfc822'
      p "Its now: #{ext}"
      name = "#{name}.#{ext}"
    end
    name
  end

  def get_message_id
    part = message_part_body.message_part
    while(part.message.nil?)
      part = part.message_part
    end
    part.message.unique_id
  end

  def mime_type
    message_part_body.message_part.mime_type
  end

  def encoding
    message_part_body.message_part.headers["Content-Transfer-Encoding"]
  end

  def download_attachment
    if file_file_name.nil? || !File.exists?(file.path)
      Delayed::Job.enqueue AttachmentDownloadJob.new(id, unique_id, get_message_id, filename, encoding, current_user)
    end
  end
end
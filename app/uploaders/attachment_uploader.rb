class AttachmentUploader < CarrierWave::Uploader::Base
  storage :file

  include CarrierWave::Compatibility::Paperclip

  def move_to_cache
    true
  end

  def move_to_store
    true
  end

  def paperclip_path
    ":rails_root/public/system/:class/:attachment/:id_partition/:style/:basename.:extension"
  end

  def store_dir
    "public/system/:class/:attachment/:id_partition/:style/:basename.:extension"
  end

  def full_filename(for_file)
    for_file
  end
end